﻿using Domain.BusinessModel;
using Domain.ModelDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IProductAP
    {
        ///// <summary>
        ///// Retrive all products whether they're active or not.
        ///// </summary>
        ///// <returns>List of products <see cref="ProductDTO"></returns>
        //Task<Response<ProductDTO>> getProducts();
        ///// <summary>
        ///// Retrive an especific product from his Id.
        ///// </summary>
        ///// <param name="productId">Product Id to be retrieved.</param>
        ///// <returns>An object of product <see cref="ProductDTO"/></returns>
        //Task<Response<ProductDTO>> getProduct(int productId);
        ///// <summary>
        ///// Save a new product in the database.
        ///// </summary>
        ///// <param name="product">Product object wich will be saved.</param>
        ///// <returns>Object <see cref="ProductDTO"/> saved.</returns>
        //Task<Response<bool>> saveProduct(ProductDTO product);
        ///// <summary>
        ///// Update a product.
        ///// </summary>
        ///// <param name="product">Product object wich will be updated.</param>
        ///// <returns>A boolean that stipulates the updating status.</returns>
        //Task<Response<bool>> updateProduct(ProductDTO product);
    }
}
