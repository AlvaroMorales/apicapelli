﻿using Domain.BusinessModel;
using Domain.ModelDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IEmployeeAP
    {
        /// <summary>
        /// Retrive all categories whether they're active or not.
        /// </summary>
        /// <returns>List of categories <see cref="EmployeeDTO"></returns>
        Task<Response<List<EmployeeDTO>>> getEmployees();
        
        /// <summary>
        /// Retrive an especific employee from his Id.
        /// </summary>
        /// <param name="employeeId">Employee Id to be retrieved.</param>
        /// <returns>An object of employee <see cref="EmployeeDTO"/></returns>
        Task<Response<EmployeeDTO>> getEmployee(int employeeId);
        /// <summary>
        /// Save a new employee in the database.
        /// </summary>
        /// <param name="employee">Employee object wich will be saved.</param>
        /// <returns>Object <see cref="EmployeeDTO"/> saved.</returns>
        Task<Response<bool>> saveEmployee(EmployeeDTO employee);
        /// <summary>
        /// Update a employee.
        /// </summary>
        /// <param name="employee">Employee object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        Task<Response<bool>> updateEmployee(EmployeeDTO employee);
    }
}
