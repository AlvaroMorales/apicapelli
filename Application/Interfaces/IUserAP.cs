﻿using Domain.BusinessModel;
using Domain.ModelDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IUserAP
    {
        /// <summary>
        /// Retrive all users whether they're active or not.
        /// </summary>
        /// <returns>List of users <see cref="UserDTO"></returns>
        Task<List<UserDTO>> getUsers();
        /// <summary>
        /// Authenticate an user.
        /// </summary>
        /// <param name="user">User to authenticate</param>
        /// <returns>Authenticated user <see cref="UserDTO"/>.</returns>
        Task<Response<UserDTO>> authenticate(UserDTO user);
        /// <summary>
        /// Retrive an especific service from his Id.
        /// </summary>
        /// <param name="serviceId">Service Id to be retrieved.</param>
        /// <returns>An object of service <see cref="UserDTO"/></returns>
        Task<Response<bool>> saveUser(UserDTO user);
    }
}
