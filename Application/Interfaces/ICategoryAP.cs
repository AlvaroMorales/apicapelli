﻿using Domain.BusinessModel;
using Domain.ModelDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICategoryAP
    {
        /// <summary>
        /// Retrive all categories whether they're active or not.
        /// </summary>
        /// <returns>List of categories <see cref="CategoryDTO"></returns>
        Task<Response<List<CategoryDTO>>> getCategories();
        /// <summary>
        /// Retrive an especific category from his Id.
        /// </summary>
        /// <param name="categoryId">Category Id to be retrieved.</param>
        /// <returns>An object of category <see cref="CategoryDTO"/></returns>
        Task<Response<CategoryDTO>> getCategory(int categoryId);
        /// <summary>
        /// Save a new category in the database.
        /// </summary>
        /// <param name="category">Category object wich will be saved.</param>
        /// <returns>Object <see cref="CategoryDTO"/> saved.</returns>
        Task<Response<bool>> saveCategory(CategoryDTO category);
        /// <summary>
        /// Update a category.
        /// </summary>
        /// <param name="category">Category object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        Task<Response<bool>> updateCategory(CategoryDTO category);
    }
}
