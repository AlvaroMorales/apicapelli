﻿using Domain.BusinessModel;
using Domain.ModelDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IServiceAP
    {
        /// <summary>
        /// Retrive all categories whether they're active or not.
        /// </summary>
        /// <returns>List of categories <see cref="ServiceDTO"></returns>
        Task<Response<List<ServiceDTO>>> getServices();
        /// <summary>
        /// Retrive all categories whether they're active or not.
        /// </summary>
        /// <param name="category">Category id.</param>
        /// <returns>List of categories <see cref="ServiceDTO"></returns>
        Task<Response<List<ServiceDTO>>> getServicesByCategory(int category);
        /// <summary>
        /// Retrive an especific service from his Id.
        /// </summary>
        /// <param name="serviceId">Service Id to be retrieved.</param>
        /// <returns>An object of service <see cref="ServiceDTO"/></returns>
        Task<Response<ServiceDTO>> getService(int serviceId);
        /// <summary>
        /// Save a new service in the database.
        /// </summary>
        /// <param name="service">Service object wich will be saved.</param>
        /// <returns>Object <see cref="ServiceDTO"/> saved.</returns>
        Task<Response<bool>> saveService(ServiceDTO service);
        /// <summary>
        /// Update a service.
        /// </summary>
        /// <param name="service">Service object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        Task<Response<bool>> updateService(ServiceDTO service);
    }
}
