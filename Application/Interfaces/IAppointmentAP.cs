﻿using Domain.BusinessModel;
using Domain.ModelDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IAppointmentAP
    {
        /// <summary>
        /// Retrive all categories whether they're active or not.
        /// </summary>
        /// <returns>List of categories <see cref="AppointmentDTO"></returns>
        Task<Response<List<AppointmentDTO>>> getAppointments();
        /// <summary>
        /// Retrive all apponitment of an user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>List of categories <see cref="AppointmentDTO"></returns>
        Task<Response<List<AppointmentDTO>>> getAppointmentsByCustomer(int userId);
        /// <summary>
        /// Retrive all apponitment of an employee.
        /// </summary>
        /// <param name="employeeId">Employee id.</param>
        /// <returns>List of categories <see cref="AppointmentDTO"></returns>
        Task<Response<List<AppointmentDTO>>> getAppointmentsByEmployee(int employeeId);

        /// <summary>
        /// Retrive an especific appointment from his Id.
        /// </summary>
        /// <param name="appointmentId">Appointment Id to be retrieved.</param>
        /// <returns>An object of appointment <see cref="AppointmentDTO"/></returns>
        Task<Response<AppointmentDTO>> getAppointment(int appointmentId);
        /// <summary>
        /// Save a new appointment in the database.
        /// </summary>
        /// <param name="appointment">Appointment object wich will be saved.</param>
        /// <returns>Object <see cref="AppointmentDTO"/> saved.</returns>
        Task<Response<bool>> saveAppointment(AppointmentDTO appointment);
        /// <summary>
        /// Update an appointment.
        /// </summary>
        /// <param name="appointment">Appointment object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        Task<Response<bool>> updateAppointment(AppointmentDTO appointment);

        /// <summary>
        /// Check the employee' and customer' disponibility.
        /// </summary>
        /// <param name="appointment">Appointment object wich will be checked.</param>
        /// <returns>A boolean that stipulates the checking task.</returns>
        Task<Response<bool>> checkDisponibility(AppointmentDTO appointment);

    }
}
