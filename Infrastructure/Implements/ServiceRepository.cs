﻿using Application.Interfaces;
using AutoMapper;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements
{
    public class ServiceRepository : IServiceAP
    {
        private readonly AGENDAContext _context;
        private readonly IMapper _mapper;
        private readonly DbSet<Service> _dbSet;

        public ServiceRepository(AGENDAContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
            _dbSet = context.Set<Service>();
        }
        /// <summary>
        /// Retrive all services whether they're active or not.
        /// </summary>
        /// <returns>List of services <see cref="ServiceDTO"></returns>
        public async Task<Response<List<ServiceDTO>>> getServices()
        {
            var response = new Response<List<ServiceDTO>>();
            try
            {
                List<Service> services = await _dbSet.Include(x => x.Category).Where(x => !x.Deleted.Value).ToListAsync();
                response.Data = _mapper.Map<List<ServiceDTO>>(services);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<List<ServiceDTO>>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Retrive all services whether they're active or not.
        /// </summary>
        /// <returns>List of services <see cref="ServiceDTO"></returns>
        public async Task<Response<List<ServiceDTO>>> getServicesByCategory(int category)
        {
            var response = new Response<List<ServiceDTO>>();
            try
            {
                List<Service> services = await _dbSet.Include(x => x.Category).Where(x => x.CategoryId == category && !x.Deleted.Value).ToListAsync();
                response.Data = _mapper.Map<List<ServiceDTO>>(services);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<List<ServiceDTO>>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        
        /// <summary>
        /// Retrive an especific service from his Id.
        /// </summary>
        /// <param name="serviceId">Service Id to be retrieved.</param>
        /// <returns>An object of service <see cref="ServiceDTO"/></returns>
        public async Task<Response<ServiceDTO>> getService(int serviceId)
        {
            var response = new Response<ServiceDTO>();
            try
            {
                Service service = await _dbSet.Include(x => x.Category).Where(x => x.ServiceId == serviceId).FirstOrDefaultAsync();
                response.Data = _mapper.Map<ServiceDTO>(service);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<ServiceDTO>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Save a new service in the database.
        /// </summary>
        /// <param name="service">Service object wich will be saved.</param>
        /// <returns>Object <see cref="ServiceDTO"/> saved.</returns>
        public async Task<Response<bool>> saveService(ServiceDTO service)
        {
            var response = new Response<bool>();
            try
            {
                Service model = _mapper.Map<Service>(service);
                await _dbSet.AddAsync(model);
                response.Data = await _context.SaveChangesAsync() > 0;
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Update a service.
        /// </summary>
        /// <param name="service">Service object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        public async Task<Response<bool>> updateService(ServiceDTO service)
        {

            var response = new Response<bool>();
            try
            {
                var exists = await _dbSet.FindAsync(service.ServiceId);
                if (exists != null)
                {
                    _mapper.Map(service, exists);
                    _dbSet.Update(exists);
                    response.Data = await _context.SaveChangesAsync() > 0;
                    response.Code = true;
                    response.Message = "Ok";
                    return response;
                }
                else
                {
                    throw new Exception($"Elemento {service.Name} no encontrado.");
                }
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
    }
}
