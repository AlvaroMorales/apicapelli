﻿using Application.Interfaces;
using AutoMapper;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Infrastructure.Model;
using Infrastructure.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements
{
    public class CategoryRepository : ICategoryAP
    {
        private readonly AGENDAContext _context;
        private readonly IMapper _mapper;
        private readonly DbSet<Category> _dbSet;

        public CategoryRepository(AGENDAContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
            _dbSet = context.Set<Category>();
        }
        /// <summary>
        /// Retrive all categories whether they're active or not.
        /// </summary>
        /// <returns>List of categories <see cref="CategoryDTO"></returns>
        public async Task<Response<List<CategoryDTO>>> getCategories()
        {
            var response = new Response<List<CategoryDTO>>();
            try
            {
                List<Category> categories = await _dbSet.Include(x => x.Service).Where(x=> !x.Deleted.Value).ToListAsync();
                response.Data = _mapper.Map<List<CategoryDTO>>(categories);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<List<CategoryDTO>>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Retrive an especific category from his Id.
        /// </summary>
        /// <param name="categoryId">Category Id to be retrieved.</param>
        /// <returns>An object of category <see cref="CategoryDTO"/></returns>
        public async Task<Response<CategoryDTO>> getCategory(int categoryId)
        {
            var response = new Response<CategoryDTO>();
            try
            {
                Category category = await _dbSet.Include(x => x.Service).Where(x => x.CategoryId == categoryId).FirstOrDefaultAsync();
                response.Data = _mapper.Map<CategoryDTO>(category);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<CategoryDTO>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Save a new category in the database.
        /// </summary>
        /// <param name="category">Category object wich will be saved.</param>
        /// <returns>Object <see cref="CategoryDTO"/> saved.</returns>
        public async Task<Response<bool>> saveCategory(CategoryDTO category)
        {
            var response = new Response<bool>();
            try
            {
                Category model = _mapper.Map<Category>(category);
                await _dbSet.AddAsync(model);
                response.Data = await _context.SaveChangesAsync() > 0;
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Update a category.
        /// </summary>
        /// <param name="category">Category object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        public async Task<Response<bool>> updateCategory(CategoryDTO category)
        {
            var response = new Response<bool>();
            try
            {
                var exists = await _context.Category.FindAsync(category.CategoryId);
                if (exists != null)
                {
                    _mapper.Map(category, exists);
                    _dbSet.Update(exists);
                    response.Data = await _context.SaveChangesAsync() > 0;
                    response.Code = true;
                    response.Message = "Ok";
                    return response;
                }
                else
                {
                    throw new Exception($"Elemento {category.Name} no encontrado.");
                }

            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
    }
}
