﻿using Application.Interfaces;
using AutoMapper;
using Domain.ModelDTO;
using Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Implements
{
    public class ProductRepository : IProductAP
    {
        private readonly AGENDAContext _dbContext;
        private readonly IMapper _mapper;
        private readonly DbSet<Product> _dbSet;

        public ProductRepository(AGENDAContext dbContext, IMapper mapper)
        {
            this._dbContext = dbContext;
            this._mapper = mapper;
            this._dbSet = dbContext.Set<Product>();
        }
        ///// <summary>
        ///// Retrive all ´products whether they're active or not.
        ///// </summary>
        ///// <returns>List of products <see cref="ProductDTO"></returns>
        //public async Task<List<ProductDTO>> getProducts()
        //{
        //    List<Product> products = await _dbSet.Include(x => x.Category).ToListAsync();
        //    return _mapper.Map<List<ProductDTO>>(products);
        //}
        ///// <summary>
        ///// Retrive an especific product from his Id.
        ///// </summary>
        ///// <param name="productId">Product Id to be retrieved.</param>
        ///// <returns>An object of product <see cref="ProductDTO"/></returns>
        //public async Task<ProductDTO> getProduct(int productId)
        //{
        //    Product product = await _dbSet.Include(x => x.Category).Where(x => x.ProductId == productId).FirstOrDefaultAsync();
        //    return _mapper.Map<Product,ProductDTO>(product);
        //}
        ///// <summary>
        ///// Save a new product in the database.
        ///// </summary>
        ///// <param name="product">Product object wich will be saved.</param>
        ///// <returns>Object <see cref="ProductDTO"/> saved.</returns>
        //public async Task<bool> saveProduct(ProductDTO product)
        //{
        //    Product model = _mapper.Map<Product>(product);
        //    await _dbSet.AddAsync(model);
        //    return await _dbContext.SaveChangesAsync() > 0;
        //}
        ///// <summary>
        ///// Update a product.
        ///// </summary>
        ///// <param name="product">Product object wich will be updated.</param>
        ///// <returns>A boolean that stipulates the updating status.</returns>
        //public async Task<bool> updateProduct(ProductDTO product)
        //{
        //    var exists = await _dbSet.FindAsync(product.ProductId);
        //    if (exists != null)
        //    {
        //        Product model = _mapper.Map<Product>(product);
        //        _dbSet.Update(model);
        //        return await _dbContext.SaveChangesAsync() > 0;
        //    }
        //    else
        //    {
        //        throw new Exception($"Elemento {product.Name} no encontrado.");
        //    }
        //}

    }
}
