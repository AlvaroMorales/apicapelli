﻿using Application.Interfaces;
using AutoMapper;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements
{
    public class UserRepository : IUserAP
    {
        private readonly AGENDAContext _context;
        private readonly IMapper _mapper;
        private readonly DbSet<User> _dbSet;

        public UserRepository(AGENDAContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
            _dbSet = context.Set<User>();
        }
        public async Task<List<UserDTO>> getUsers()
        {
            List<User> users = await _dbSet.ToListAsync();
            return _mapper.Map<List<UserDTO>>(users);
        }

        /// <summary>
        /// Save a new category in the database.
        /// </summary>
        /// <param name="user">Category object wich will be saved.</param>
        /// <returns>Object <see cref="UserDTO"/> saved.</returns>
        public async Task<Response<UserDTO>> authenticate(UserDTO user)
        {
            var response = new Response<UserDTO>();
            try
            {
                User wantedUser = await _dbSet.FirstOrDefaultAsync(x => x.Email == user.Email && x.Password == user.Password);
                if (wantedUser != null)
                {
                    response.Data = _mapper.Map<UserDTO>(wantedUser);
                    response.Code = true;
                    response.Message = "Usuario autenticado correctamente.";
                }
                else
                {
                    response.Data = null;
                    response.Code = false;
                    response.Message = "Usuario inválido.";
                }
                return response;
            }
            catch (Exception ex)
            {
                return new Response<UserDTO>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }

        }

        public async Task<Response<bool>> saveUser(UserDTO user)
        {
            Response<bool> response = new Response<bool>();
            try
            {
                if (string.IsNullOrWhiteSpace(user.Person.FirstName) || string.IsNullOrWhiteSpace(user.Person.LastName) || string.IsNullOrWhiteSpace(user.Email) || string.IsNullOrWhiteSpace(user.Password) || !user.Person.Phone.Any())
                    throw new Exception("Debe completar los campos");

                if (_dbSet.Any(x => x.Email.Equals(user.Email.ToLower())) || _context.Person.Any(x => x.Email.Equals(user.Email.ToLower())))
                    throw new Exception("El email dado ya existe.");

                var personModel = _mapper.Map<Person>(user.Person);
                await _context.Person.AddAsync(personModel);
                await _context.SaveChangesAsync();
                if (user.UserType == 1)
                {
                    var employeeModel = new Employee() {
                        EmployeeId = 0,
                        PersonId = personModel.PersonId, 
                        Deleted = false, 
                        UpdatedAt = DateTime.Now,
                        CreatedAt = DateTime.Now, 
                        EmployeeTypeId = 1, 
                        CreatedBy = user.CreatedBy, 
                        UpdatedBy = user.UpdatedBy };
                    await _context.Employee.AddAsync(employeeModel);
                    await _context.SaveChangesAsync();
                    user.UserReference = employeeModel.EmployeeId;
                }
                else
                {
                    var customerModel = new Customer()
                    {
                        CustomerId = 0,
                        PersonId = personModel.PersonId,
                        Deleted = false,
                        UpdatedAt = DateTime.Now,
                        CreatedAt = DateTime.Now,
                        CreatedBy = user.CreatedBy,
                        UpdatedBy = user.UpdatedBy
                    };
                    await _context.Customer.AddAsync(customerModel);
                    await _context.SaveChangesAsync();
                    user.UserReference = customerModel.CustomerId;
                }

                var userModel = _mapper.Map<User>(user);
                await _dbSet.AddAsync(userModel);
               
                response.Code = true;
                response.Data = await _context.SaveChangesAsync() > 0;
                response.Message = "Usuario creado correctamente";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
    }
}
