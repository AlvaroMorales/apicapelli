﻿using Application.Interfaces;
using AutoMapper;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements
{
    public class AppointmentRepository : IAppointmentAP
    {
        private readonly AGENDAContext _context;
        private readonly IMapper _mapper;
        private readonly DbSet<Appointment> _dbSet;

        public AppointmentRepository(AGENDAContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
            _dbSet = context.Set<Appointment>();
        }
        /// <summary>
        /// Retrive all appointments whether they're active or not.
        /// </summary>
        /// <returns>List of appointments <see cref="AppointmentDTO"></returns>
        public async Task<Response<List<AppointmentDTO>>> getAppointments()
        {
            var response = new Response<List<AppointmentDTO>>();
            try
            {
                List<Appointment> appointments = await _dbSet.Include(x => x.AppointmentService).Where(x => !x.Deleted.Value).ToListAsync();
                response.Data = _mapper.Map<List<AppointmentDTO>>(appointments);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<List<AppointmentDTO>>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Retrive all apponitment of an user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>List of categories <see cref="AppointmentDTO"></returns>
        public async Task<Response<List<AppointmentDTO>>> getAppointmentsByCustomer(int userId)
        {
            var response = new Response<List<AppointmentDTO>>();
            try
            {
                List<Appointment> appointments = await _dbSet.Include(x => x.AppointmentService).Where(x => x.CustomerId == x.CustomerId && !x.Deleted.Value).ToListAsync();
                response.Data = _mapper.Map<List<AppointmentDTO>>(appointments);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<List<AppointmentDTO>>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Retrive all apponitment of an employee.
        /// </summary>
        /// <param name="employeeId">Employee id.</param>
        /// <returns>List of categories <see cref="AppointmentDTO"></returns>
        public async Task<Response<List<AppointmentDTO>>> getAppointmentsByEmployee(int employeeId)
        {
            var response = new Response<List<AppointmentDTO>>();
            try
            {
                List<Appointment> appointments = await _dbSet.Include(x => x.AppointmentService).Where(x => x.AppointmentService.Any(y => y.EmployeeId == employeeId) && !x.Deleted.Value).ToListAsync();
                response.Data = _mapper.Map<List<AppointmentDTO>>(appointments);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<List<AppointmentDTO>>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }

        /// <summary>
        /// Retrive an especific appointment from his Id.
        /// </summary>
        /// <param name="appointmentId">Appointment Id to be retrieved.</param>
        /// <returns>An object of appointment <see cref="AppointmentDTO"/></returns>
        public async Task<Response<AppointmentDTO>> getAppointment(int appointmentId)
        {
            var response = new Response<AppointmentDTO>();
            try
            {
                Appointment appointment = await _dbSet.Include(x => x.AppointmentService).Where(x => x.AppointmentId == appointmentId).FirstOrDefaultAsync();
                response.Data = _mapper.Map<AppointmentDTO>(appointment);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<AppointmentDTO>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Save a new appointment in the database.
        /// </summary>
        /// <param name="appointment">Appointment object wich will be saved.</param>
        /// <returns>Object <see cref="AppointmentDTO"/> saved.</returns>
        public async Task<Response<bool>> saveAppointment(AppointmentDTO appointment)
        {
            var response = new Response<bool>();
            try
            {
                var isAvailable = await checkDisponibility(appointment);
                if (!isAvailable.Code)
                {
                    return isAvailable;
                }
                Appointment model = _mapper.Map<Appointment>(appointment);
                await _dbSet.AddAsync(model);
                response.Data = await _context.SaveChangesAsync() > 0;
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Update a appointment.
        /// </summary>
        /// <param name="appointment">Appointment object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        public async Task<Response<bool>> updateAppointment(AppointmentDTO appointment)
        {

            var response = new Response<bool>();
            try
            {
                var exists = await _dbSet.FindAsync(appointment.AppointmentId);
                if (exists != null)
                {
                    _mapper.Map(appointment, exists);
                    _dbSet.Update(exists);
                    response.Data = await _context.SaveChangesAsync() > 0;
                    response.Code = true;
                    response.Message = "Ok";
                    return response;
                }
                else
                {
                    throw new Exception($"Elemento no encontrado.");
                }
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Check the employee' and customer' disponibility.
        /// </summary>
        /// <param name="appointment">Appointment object wich will be checked.</param>
        /// <returns>A boolean that stipulates the checking task.</returns>
        public async Task<Response<bool>> checkDisponibility(AppointmentDTO appointment)
        {
            var result = new Response<bool>();
            bool isAvailable = true;
            var start = appointment.AppointmentService.FirstOrDefault().ServiceDate;
            var end = appointment.AppointmentService.FirstOrDefault().ServiceDate;

            foreach (var appointmentService in appointment.AppointmentService)
            {
                var service = await _context.Service.FindAsync(appointmentService.ServiceId);
                if (start > appointmentService.ServiceDate.Value)
                {
                    start = appointmentService.ServiceDate.Value;
                }
                if (end < appointmentService.ServiceDate.Value.Add(service.Duration.Value))
                {
                    end = appointmentService.ServiceDate.Value.Add(service.Duration.Value);
                }


                var isEmployeeAvailable = (await _context.AppointmentService.Where(x => !x.Deleted.Value && x.Appointment.Status != 3 && x.Appointment.Status != 4).AnyAsync(x => x.EmployeeId == appointmentService.EmployeeId
                                                                                    && (
                                                                                           (x.ServiceDate > appointmentService.ServiceDate && x.ServiceDate < appointmentService.ServiceDate.Value.Add(service.Duration.Value))
                                                                                        || (x.ServiceDate.Value.Add(x.ServiceTimeSupposed.Value) > appointmentService.ServiceDate && x.ServiceDate.Value.Add(x.ServiceTimeSupposed.Value) < appointmentService.ServiceDate.Value.Add(service.Duration.Value))
                                                                                       )
                                                                                    ))!;
                if (isEmployeeAvailable)
                {
                    var employee = await _context.Employee.FindAsync(appointmentService.EmployeeId);
                    return new Response<bool>
                    {
                        Code = false,
                        Data = false,
                        Message = $"El empleado {employee.Person.FirstName} {employee.Person.LastName} ya tiene un cita asignada entre {appointmentService.ServiceDate.Value.ToLongDateString()} y {appointmentService.ServiceDate.Value.Add(service.Duration.Value).ToLongDateString()}"
                    };
                }
            }

            var isCustomerAvailable = (await _context.Appointment.Where(x => !x.Deleted.Value && x.Status != 3 && x.Status != 4).AnyAsync(x => x.CustomerId == appointment.CustomerId && (
                                                                          (x.AppointmentDate > start && x.AppointmentDate < end)) || (x.AppointmentDateEnd > start && x.AppointmentDateEnd < end))
                                                                        )!;
            if (isCustomerAvailable)
            {
                var customer = await _context.Customer.FindAsync(appointment.CustomerId);
                return new Response<bool>
                {
                    Code = false,
                    Data = false,
                    Message = $"El empleado {customer.Person.FirstName} {customer.Person.LastName} ya tiene un cita asignada entre {start?.ToLongDateString()} y {end?.ToLongDateString()}."
                };
            }
            return new Response<bool>()
            {
                Code = true,
                Data = true,
                Message = "Disponible"
            };
        }
    }
}
