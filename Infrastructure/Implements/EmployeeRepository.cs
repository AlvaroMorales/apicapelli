﻿using Application.Interfaces;
using AutoMapper;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements
{
    public class EmployeeRepository : IEmployeeAP
    {
        private readonly AGENDAContext _context;
        private readonly IMapper _mapper;
        private readonly DbSet<Employee> _dbSet;

        public EmployeeRepository(AGENDAContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
            _dbSet = context.Set<Employee>();
        }
        /// <summary>
        /// Retrive all employees whether they're active or not.
        /// </summary>
        /// <returns>List of employees <see cref="EmployeeDTO"></returns>
        public async Task<Response<List<EmployeeDTO>>> getEmployees()
        {
            var response = new Response<List<EmployeeDTO>>();
            try
            {
                List<Employee> employees = await _dbSet.Include(x => x.Person).Where(x => !x.Deleted.Value).ToListAsync();
                response.Data = _mapper.Map<List<EmployeeDTO>>(employees);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<List<EmployeeDTO>>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }

        /// <summary>
        /// Retrive an especific employee from his Id.
        /// </summary>
        /// <param name="employeeId">Employee Id to be retrieved.</param>
        /// <returns>An object of employee <see cref="EmployeeDTO"/></returns>
        public async Task<Response<EmployeeDTO>> getEmployee(int employeeId)
        {
            var response = new Response<EmployeeDTO>();
            try
            {
                Employee employee = await _dbSet.Include(x => x.Person).Where(x => x.EmployeeId == employeeId).FirstOrDefaultAsync();
                response.Data = _mapper.Map<EmployeeDTO>(employee);
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<EmployeeDTO>()
                {
                    Code = false,
                    Data = null,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Save a new employee in the database.
        /// </summary>
        /// <param name="employee">Employee object wich will be saved.</param>
        /// <returns>Object <see cref="EmployeeDTO"/> saved.</returns>
        public async Task<Response<bool>> saveEmployee(EmployeeDTO employee)
        {
            var response = new Response<bool>();
            try
            {
                Employee model = _mapper.Map<Employee>(employee);
                await _dbSet.AddAsync(model);
                response.Data = await _context.SaveChangesAsync() > 0;
                response.Code = true;
                response.Message = "Ok";
                return response;
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
        /// <summary>
        /// Update a employee.
        /// </summary>
        /// <param name="employee">Employee object wich will be updated.</param>
        /// <returns>A boolean that stipulates the updating status.</returns>
        public async Task<Response<bool>> updateEmployee(EmployeeDTO employee)
        {

            var response = new Response<bool>();
            try
            {
                var exists = await _dbSet.FindAsync(employee.EmployeeId);
                if (exists != null)
                {
                    _mapper.Map(employee, exists);
                    _dbSet.Update(exists);
                    response.Data = await _context.SaveChangesAsync() > 0;
                    response.Code = true;
                    response.Message = "Ok";
                    return response;
                }
                else
                {
                    throw new Exception($"Elemento no encontrado.");
                }
            }
            catch (Exception ex)
            {
                return new Response<bool>()
                {
                    Code = false,
                    Data = false,
                    Message = $"Error: {ex.Message}"
                };
            }
        }
    }
}
