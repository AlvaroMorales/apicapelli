﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Model
{
    /// <summary>
    /// Permite especificar las clases que son permitidas utilizar para los repositorios relacionados con el DbContext
    /// </summary>
    public interface IEntity { }
}
