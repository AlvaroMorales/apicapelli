﻿using Microsoft.Extensions.DependencyInjection;
namespace ApiCapelli
{
    public static class DependenciesConfiguration
    {
        /// <summary>
        /// Adds the dependency injections.
        /// </summary>
        /// <param name="services">The services.</param>
        public static void AddDependencyInjections(this IServiceCollection services) { }
    }
}
