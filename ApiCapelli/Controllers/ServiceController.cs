﻿using ApiCapelli.Filters;
using Application.Interfaces;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCapelli.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApikeyAuth]
    public class ServiceController : ControllerBase
    {
        private readonly IServiceAP _service;

        public ServiceController(IServiceAP service)
        {
            this._service = service;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Response<List<ServiceDTO>>), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetService()
        {
            try
            {
                var result = await _service.getServices();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }

        [HttpGet("ServicesByCategory/{categoryId}")]
        [ProducesResponseType(typeof(Response<List<ServiceDTO>>), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetServiceByCategory(int categoryId)
        {
            try
            {
                var result = await _service.getServicesByCategory(categoryId);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Response<ServiceDTO>), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetService(int id)
        {
            try
            {
                var result = await _service.getService(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpPost]
        [ProducesResponseType(typeof(Response<ServiceDTO>), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> SaveService(ServiceDTO service)
        {
            try
            {
                var result = await _service.saveService(service);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpPut]
        [ProducesResponseType(typeof(Response<ServiceDTO>), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> UpdateService(ServiceDTO service)
        {
            try
            {
                var result = await _service.updateService(service);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
    }
}
