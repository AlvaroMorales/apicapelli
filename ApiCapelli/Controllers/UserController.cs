﻿using ApiCapelli.Filters;
using Application.Interfaces;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCapelli.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApikeyAuth]
    public class UserController : ControllerBase
    {
        private readonly IUserAP _user;

        public UserController(IUserAP user)
        {
            this._user = user;
        }
        /// <summary>
        /// Register a new user (employee or customer).
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(Response<bool>), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> SaveUser(UserDTO user)
        {
            try
            {
                var result = await _user.saveUser(user);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        /// <summary>
        /// Autenticate and user (employee or customer) by his email and password.
        /// </summary>
        /// <param name="user">User to autenticate.</param>
        /// <returns><see cref="Response{T}"/></returns>
        [HttpPost("Authenticate")]
        [ProducesResponseType(typeof(Response<UserDTO>), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> Authenticate(UserDTO user)
        {
            try
            {
                var result = await _user.authenticate(user);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
    }
}
