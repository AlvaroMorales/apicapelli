﻿using ApiCapelli.Filters;
using Application.Interfaces;
using Domain.ModelDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCapelli.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApikeyAuth]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeAP _employee;

        public EmployeeController(IEmployeeAP employee)
        {
            this._employee = employee;
        }

        [HttpGet]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetEmployee()
        {
            try
            {
                var result = await _employee.getEmployees();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetEmployee(int id)
        {
            try
            {
                var result = await _employee.getEmployee(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpPost]
        [ProducesResponseType(typeof(EmployeeDTO), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> SaveEmployee(EmployeeDTO employee)
        {
            try
            {
                var result = await _employee.saveEmployee(employee);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpPut]
        [ProducesResponseType(typeof(EmployeeDTO), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> UpdateEmployee(EmployeeDTO employee)
        {
            try
            {
                var result = await _employee.updateEmployee(employee);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
    }
}
