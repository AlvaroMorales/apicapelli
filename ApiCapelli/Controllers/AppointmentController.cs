﻿using ApiCapelli.Filters;
using Application.Interfaces;
using Domain.BusinessModel;
using Domain.ModelDTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCapelli.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApikeyAuth]
    public class AppointmentController : ControllerBase
    {
        private readonly IAppointmentAP _appointment;

        public AppointmentController(IAppointmentAP appointment)
        {
            this._appointment = appointment;
        }
        /// <summary>
        /// Retrive all appointments.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(Response<AppointmentDTO>), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetAppointment()
        {
            try
            {
                var result = await _appointment.getAppointments();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        /// <summary>
        /// Retrieve an appointment.
        /// </summary>
        /// <param name="id">Appointment id to retrieve.</param>
        /// <returns><see cref="Response{AppointmentDTO}"/>></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Response<List<AppointmentDTO>>), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetAppointment(int id)
        {
            try
            {
                var result = await _appointment.getAppointment(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        /// <summary>
        /// Retrive all apponitment of an user.        /// </summary>
        /// <param name="id">Appointment id to retrieve.</param>
        /// <returns><see cref="Response{AppointmentDTO}"/>></returns>
        [HttpGet("AppoinmentsByCustomer/{id}")]
        [ProducesResponseType(typeof(Response<List<AppointmentDTO>>), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetAppointmentsByCustomer(int id)
        {
            try
            {
                var result = await _appointment.getAppointmentsByCustomer(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        /// <summary>
        /// Retrive all apponitment of an employee.
        /// </summary>
        /// <param name="id">Appointment id to retrieve.</param>
        /// <returns><see cref="Response{AppointmentDTO}"/>></returns>
        [HttpGet("AppoinmentsByEmployee/{id}")]
        [ProducesResponseType(typeof(Response<List<AppointmentDTO>>), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetAppointmentsByEmployee(int id)
        {
            try
            {
                var result = await _appointment.getAppointmentsByEmployee(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        /// <summary>
        /// Save and apponitment.
        /// </summary>
        /// <param name="appointment">Appointment to save.</param>
        /// <returns>Bool that indicates the saving status.</returns>
        [HttpPost]
        [ProducesResponseType(typeof(Response<bool>), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> SaveAppointment(AppointmentDTO appointment)
        {
            try
            {
                var result = await _appointment.saveAppointment(appointment);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        /// <summary>
        /// Send a notification to the user of the given appointment.
        /// </summary>
        /// <param name="appointment"><see cref="AppointmentDTO"/>></param>
        /// <returns>The notification status.</returns>
        [HttpPost("SendNotification")]
        [ProducesResponseType(typeof(Response<bool>), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> SendNotification(AppointmentDTO appointment)
        {
            try
            {
                var result = new Response<bool> { Code = true, Data = true, Message = "Ok" };
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        } 
        /// <summary>
        /// Update and appointment.
        /// </summary>
        /// <param name="appointment">Appointment to save.</param>
        /// <returns>Bool that indicates the updating status.</returns>
        [HttpPut]
        [ProducesResponseType(typeof(Response<AppointmentDTO>), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> UpdateAppointment(AppointmentDTO appointment)
        {
            try
            {
                var result = await _appointment.updateAppointment(appointment);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
    }
}

