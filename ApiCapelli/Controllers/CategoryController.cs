﻿using ApiCapelli.Filters;
using Application.Interfaces;
using Domain.ModelDTO;
using Infrastructure.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiCapelli.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ApikeyAuth]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryAP _category;

        public CategoryController(ICategoryAP category)
        {
            this._category = category;
        }

        [HttpGet]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetCategoty()
        {
            try
            {
                var result = await _category.getCategories();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        //This a test.
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> GetCategoty(int id)
        {
            try
            {
                var result = await _category.getCategory(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpPost]
        [ProducesResponseType(typeof(CategoryDTO), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> SaveCategory(CategoryDTO category)
        {
            try
            {
                var result = await _category.saveCategory(category);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
        [HttpPut]
        [ProducesResponseType(typeof(CategoryDTO), 201)]
        [ProducesResponseType(typeof(string), 400)]
        [ProducesResponseType(typeof(string), 404)]
        [ProducesResponseType(typeof(string), 500)]
        public async Task<IActionResult> UpdateCategory(CategoryDTO category)
        {
            try
            {
                var result = await _category.updateCategory(category);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest($"Ha ocurrido un error: { ex.Message }.");
            }
        }
    }
}
