﻿using AutoMapper;
using Domain.CustomModel;
using Domain.ModelDTO;
using Infrastructure.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Mapping
{
    public class UserProfile: Profile
    {
        public UserProfile()
        {
            CreateMap<Category, CategoryDTO>().ReverseMap();
            CreateMap<Category, CategoryDetail>();
            CreateMap<Product, ProductDTO>().ReverseMap();
            CreateMap<Product, ProductDetail>();
            CreateMap<Service, ServiceDTO>().ReverseMap();
            CreateMap<Service, ServiceDetail>();
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<Person, PersonDTO>().ReverseMap();
            CreateMap<Phone, PhoneDTO>().ReverseMap();
            CreateMap<Employee, EmployeeDTO>().ReverseMap();
            CreateMap<Appointment, AppointmentDTO>().ReverseMap();
            CreateMap<AppointmentService, AppointmentServiceDTO>().ReverseMap();

        }

    }
}
