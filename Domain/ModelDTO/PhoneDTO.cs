﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ModelDTO
{
    public class PhoneDTO
    {
        public int PhoneId { get; set; }
        public int? PersonId { get; set; }
        public string PhoneNumber { get; set; }
        public int? Type { get; set; }
        public bool? Favorite { get; set; }
        public decimal? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }
    }
}
