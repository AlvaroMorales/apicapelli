﻿using Domain.CustomModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ModelDTO
{
    public class CategoryDTO
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }
        public string Image { get; set; }
        public ICollection<ServiceDetail> Service { get; set; }
    }
}
