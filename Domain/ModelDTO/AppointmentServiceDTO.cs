﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ModelDTO
{
    public class AppointmentServiceDTO
    {
        public int AppointmentService1 { get; set; }
        public int? AppointmentId { get; set; }
        public int? EmployeeId { get; set; }
        public int? ServiceId { get; set; }
        public DateTime? ServiceDate { get; set; }
        public TimeSpan? ServiceTimeSupposed { get; set; }
        public TimeSpan? ServiceTimeTaken { get; set; }
        public int? Status { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }

    }
}
