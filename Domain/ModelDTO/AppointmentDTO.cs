﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ModelDTO
{
    public class AppointmentDTO
    {
        public int AppointmentId { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public DateTime? AppointmentDateEnd { get; set; }
        public int? CustomerId { get; set; }
        public int? Status { get; set; }
        public TimeSpan? ServiceTimesSupposed { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }
        public string CancelationReason { get; set; }

        public virtual ICollection<AppointmentServiceDTO> AppointmentService { get; set; }
    }
}
