﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ModelDTO
{
    public class FileTypeDTO
    {
        public int FileTypeId { get; set; }
        public bool Required { get; set; }
        public string Name { get; set; }
        public string TypeDescription { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }
    }
}
