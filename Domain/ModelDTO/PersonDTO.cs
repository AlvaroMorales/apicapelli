﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ModelDTO
{
    public class PersonDTO
    {
        public int PersonId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string SecondSurname { get; set; }
        public int? IdentificationType { get; set; }
        public string Identification { get; set; }
        public bool? Gender { get; set; }
        public string Address { get; set; }
        public string Foto { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public DateTime? Birthdate { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }

        public ICollection<PhoneDTO> Phone { get; set; }
    }
}
