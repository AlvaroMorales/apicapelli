﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.ModelDTO
{
    public class EmployeeDTO
    {
        public int EmployeeId { get; set; }
        public int? PersonId { get; set; }
        public int? EmployeeTypeId { get; set; }
        public string Code { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }

        public virtual PersonDTO Person { get; set; }
        //public virtual ICollection<AppointmentService> AppointmentService { get; set; }
    }
}
