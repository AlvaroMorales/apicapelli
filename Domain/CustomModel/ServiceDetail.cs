﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.CustomModel
{
    public class ServiceDetail
    {
        public int ServiceId { get; set; }
        public string Description { get; set; }
        public decimal? Price { get; set; }
        public TimeSpan? Duration { get; set; }
        public int? CreatedBy { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool? Deleted { get; set; }
        public string Name { get; set; }
        public int? CategoryId { get; set; }
        public string Image { get; set; }
        public int? Periodicity { get; set; }
    }
}
