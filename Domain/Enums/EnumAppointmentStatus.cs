﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Enums
{
    public enum EnumAppointmentStatus
    {
        [Display(Name = "Pendiente de aprobación")]
        PENDIENTE_APROBACION = 0,
        [Display(Name = "Aprobada/Pendiente de brindar")]
        APROBADA = 1,
        [Display(Name = "Proceso")]
        PROCESO = 2,
        [Display(Name = "Terminada")]
        TERMINADA = 3,
        [Display(Name = "Canceladas")]
        CANCELADA = 4
    }
}
